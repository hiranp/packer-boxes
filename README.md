# packer-boxes for Oracle Enterprise Linux 5

### Overview

This repository contains templates for Oracle Linux 5 that can create
Vagrant boxes using Packer.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build any of these Vagrant boxes:

  - [Packer](http://www.packer.io/)
  - [Vagrant](http://vagrantup.com/)
  - [VirtualBox](https://www.virtualbox.org/)
  - [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Building the Vagrant boxes with Packer

I am using JSON files containing user variables to build specific versions of Oracle Linux.
You tell `packer` to use a specific user variable file via the `-var-file=` command line
option.  This will override the default options on the core `ol-core.json` packer template,
which builds Oracle Linux 5 by default.

For example, to build Oracle Linux 5.11 64bit, use the following:

    $ packer build -var-file=vars/oel511.json ol-core.json

If you want to make boxes for a specific desktop virtualization platform, use the `-only`
parameter.  Right now only supporting building Oracle Linux 5.11 for VirtualBox:

    $ packer build -only=virtualbox-iso -var-file=vars/oel511.json ol-core.json

## Configuring Packer logging
$ export PACKER_LOG=1
$ export PACKER_LOG_PATH="packerbuild.log"

or use ASK on Error
packer build -on-error=ask -var-file=vars/oel511-i386.json ol-core.json