#!/bin/bash -eux

# Vagrant specific
date > /etc/vagrant_box_build_time

# @TODO - Fix chmod: cannot access `/etc/sudoers.d/vagrant': No such file or directory
sudo ln -s /usr/bin/gksudo /usr/bin/pkexec
pkexec chmod 555 /etc/sudoers
pkexec chmod 555 /etc/sudoers.d/README

# Add vagrant user
/usr/sbin/groupadd vagrant
/usr/sbin/useradd vagrant -g vagrant -G wheel
echo "vagrant" | passwd --stdin vagrant
echo "vagrant        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
chmod 0440 /etc/sudoers.d/vagrant

# Installing vagrant keys
mkdir -pm 700 /home/vagrant/.ssh
# Check if newer key works
#wget --no-check-certificate 'https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub' -O /home/vagrant/.ssh/authorized_keys
wget --no-check-certificate 'https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub' -O /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh

#TODO: fix selinux
#restorecon -R -v /home/vagrant/.ssh
