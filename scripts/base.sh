#!/bin/sh -eux
# Base install


((++idx)) && printf "\n##$idx--base------------------------------------------------------\n"
ls -ltr $HOME
ls -ltr $HOME\bin
((++idx)) && printf "\n##$idx------------------------------------------------------------\n"


# Add official mozilla certs
((++idx)) && printf "\n##$idx--certs-----------------------------------------------------\n"
/usr/local/bin/curl -k -L --output /etc/pki/tls/certs/ca-bundle.crt https://curl.haxx.se/ca/cacert.pem
# wget --secure-protocol=TLSv1_1 --no-check-certificate -O /etc/pki/tls/certs/ca-bundle.crt https://curl.haxx.se/ca/cacert.pem

# Setup Centos 5 EPEL 
((++idx)) && printf "\n##$idx--centos-epl-------------------------------------------------\n"
# Other Repos https://wiki.centos.org/AdditionalResources/Repositories
/home/vagrant/curl/bin/curl -k -L --output /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 https://archive.kernel.org/centos-vault/RPM-GPG-KEY-CentOS-5
/home/vagrant/curl/bin/curl -k -L --output /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle https://oss.oracle.com/ol6/RPM-GPG-KEY-oracle
# wget --secure-protocol=TLSv1_1 --no-check-certificate -O /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 https://archive.kernel.org/centos-vault/RPM-GPG-KEY-CentOS-5
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle

((++idx)) && printf "\n##$idx--redhat-epl-------------------------------------------------\n"
if [ ${HOSTTYPE} == "x86_64" ]; then
    /usr/local/bin/curl -k -L --output /tmp/packages/epel-release-5-4.noarch.rpm https://archives.fedoraproject.org/pub/archive/epel/5/x86_64/epel-release-5-4.noarch.rpm
    #wget --secure-protocol=TLSv1_1 --no-check-certificate -O /tmp/epel-release-5-4.noarch.rpm https://archives.fedoraproject.org/pub/archive/epel/5/x86_64/epel-release-5-4.noarch.rpm 
else
    /home/vagrant/curl/bin/curl -k -L --output /tmp/packages/epel-release-5-4.noarch.rpm http://archives.fedoraproject.org/pub/archive/epel/5/i386/epel-release-5-4.noarch.rpm
    #wget --secure-protocol=TLSv1_1 --no-check-certificate -O /tmp/epel-release-5-4.noarch.rpm http://archives.fedoraproject.org/pub/archive/epel/5/i386/epel-release-5-4.noarch.rpm
fi
rpm -Uvh /tmp/packages/epel-release-5-4.noarch.rpm
yum install epel-release -y

# Not flexible to switch between direct Internet access and behind firewall
# --httpproxy HOST --httpport PORT
# rpm -ivh http://download.fedoraproject.org/pub/epel/5/x86_64/epel-release-5-4.noarch.rpm
# rpm -Uvh --nogpgcheck  https://archives.fedoraproject.org/pub/archive/epel/5/i386/epel-release-5-4.noarch.rpm
# rpm -Uvh --nogpgcheck  https://archives.fedoraproject.org/pub/archive/epel/5/x86_64/epel-release-5-4.noarch.rpm

rpm -qa | grep epel
# Install basic packages
((++idx)) && printf "\n##$idx--install-pkgs-----------------------------------------------\n"
yum --disablerepo="*" --enablerepo="epel" list available
yum install -y /tmp/packages/pkgconfig-0.21-2.el5.i386.rpm
`which pkg-config` /usr/local/ssl/bin/openssl --run 'echo $PKG_CONFIG_PATH'
yum clean all
yum update 
yum --enablerepo=epel install -y --skip-broken htop python26 python26-devel python-virtualenv mcrypt mhash-devel expat21-devel
((++idx)) && printf "\n##$idx--check-pkgs-----------------------------------------------\n"
rpm -qlp /tmp/packages/pkgconfig-0.21-2.el5.i386.rpm
rpm -qlp openssl101e-devel.i386*
rpm -qlp python26-devel*
